package com.globallogin.android.training.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);
        findViewById(R.id.act_main_showBtn).setOnClickListener(onClickListener);
        findViewById(R.id.act_main_finishBtn).setOnClickListener(onClickListener);
        notify("create");
    }

    @Override
    protected void onStart() {
        super.onStart();
        notify("start");
    }

    @Override
    protected void onResume() {
        super.onResume();
        notify("resume");
    }

    @Override
    protected void onPause() {
        notify("pause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        notify("stop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        notify("destroy");
        super.onDestroy();
    }

    private void notify(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch(v.getId()) {
                case R.id.act_main_finishBtn:
                    onClickFinish();
                    break;
                case R.id.act_main_showBtn:
                    onClickShow();
                    break;
            }
        }

        private void onClickShow() {
            startActivity(new Intent(MainActivity.this, TransparentActivity.class));
        }

        private void onClickFinish() {
            finish();
        }
    };
}
