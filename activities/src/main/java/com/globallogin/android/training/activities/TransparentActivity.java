package com.globallogin.android.training.activities;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by curila on 27.5.2015.
 */
public class TransparentActivity extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_transparent);
    }
}
