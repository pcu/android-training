package com.globallogic.android.training.helloworld;


import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class HelloActivity extends Activity {

    private Button greetBtn;
    private EditText nameEdit;
    private boolean clicked;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_hello);
        initViews();
    }

    private void initViews() {
        greetBtn = (Button) findViewById(R.id.act_hello_greet);
        nameEdit = (EditText) findViewById(R.id.act_hello_name);
        greetBtn.setOnClickListener(onClickListener);
        updateButtonVisibility(clicked);
    }

    private void updateButtonVisibility(boolean clicked) {
//        this.clicked = clicked;
//        greetBtn.setVisibility(clicked ? View.GONE : View.VISIBLE);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {


            Editable text = nameEdit.getText();
            if(TextUtils.isEmpty(text)) {
                Toast.makeText(HelloActivity.this, "Enter name!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(HelloActivity.this, String.format("Hello %1s!", text), Toast.LENGTH_SHORT).show();
                updateButtonVisibility(true);
            }
        }
    };
}
